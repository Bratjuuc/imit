package Goods;

import java.util.Objects;

public class PiecedGoods extends Goods implements MassGetter{
    private double mass;

    public PiecedGoods(PiecedGoods goods) {
        super(goods.getName(), goods.getDescription());
        this.mass = goods.mass;
    }

    public void setMass(double mass) {
        if (mass < 0){
            throw new IllegalArgumentException();
        }
        this.mass = mass;
    }


    @Override
    public double getNettoMass() {
        return mass;
    }

    @Override
    public double getBruttoMass(){
        return mass;
    }

    public PiecedGoods(String name, String description, double mass)
    {
        super(name, description);
        setMass(mass);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PiecedGoods)) return false;
        if (!super.equals(o)) return false;
        PiecedGoods that = (PiecedGoods) o;
        return Double.compare(that.mass, mass) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mass);
    }

    @Override
    public String toString() {
        return super.toString() + "(штучный товар, масса = " + mass + " г): ";
    }
}
