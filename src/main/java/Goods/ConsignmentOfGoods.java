package Goods;

import java.util.Arrays;

public class ConsignmentOfGoods extends Goods implements MassGetter{
    private MassGetter[] consignment;

    public ConsignmentOfGoods(MassGetter ... arr){
        consignment = arr;
    }
    public ConsignmentOfGoods(String name, String description,MassGetter ... arr){
        super(name, description);
        consignment = arr;
    }

    public double getMass(){
        double sum = 0;
        for (MassGetter i: consignment){
            sum += i. getBruttoMass();
        }
        return sum;
    }

    @Override
    public  double getNettoMass(){
        return getMass();
    }

    @Override
    public  double getBruttoMass(){
        return getMass();
    }

    public int getLength(){
        return consignment.length;
    }

    public Goods goodsAt(int i){
        if (i < 0 || i >= consignment.length){
            throw new IllegalArgumentException();
        }
        return (Goods)consignment[i];
    }

    @Override
    public String toString(){
        StringBuilder rest = new StringBuilder("");
        for (MassGetter i: consignment) {
            rest.append(i.toString());
            rest.append(", ");
        }
        return super.toString() + " (партия товаров) Содержимое: " + rest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConsignmentOfGoods)) return false;
        if (!super.equals(o)) return false;
        ConsignmentOfGoods that = (ConsignmentOfGoods) o;
        return Arrays.equals(consignment, that.consignment);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(consignment);
        return result;
    }
}
