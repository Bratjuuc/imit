package Goods;

import java.util.Objects;

public class Wrapping {
    private String name;
    private double mass;

    public Wrapping(Wrapping init){
        this.mass = init.mass;
        this.name = init.name;
    }

    public Wrapping( String name,double mass){
        setMass(mass);
        setName(name);
    }

    public void setMass(double mass) {
        if (mass < 0) {
            throw new IllegalArgumentException();
        }
        this.mass = mass;
    }

    public void setName(String name) {
        if (name.length() == 0){
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Wrapping)) return false;
        Wrapping wrapping = (Wrapping) o;
        return Double.compare(wrapping.mass, mass) == 0 &&
                name.equals(wrapping.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mass);
    }

    @Override
    public String toString(){
        return "Упаковка: " + name + ", масса = " + mass + " г";
    }
}
