package Goods;

import java.util.Objects;

public class WrappedWeightGoods extends WeightGoods implements MassGetter{
    private Wrapping wrapping;
    private double mass;

    public void setWrapping(Wrapping wrapping) {
        this.wrapping = wrapping;
    }

    public Wrapping getWrapping() {
        return wrapping;
    }

    @Override
    public double getNettoMass() {
        return mass;
    }

    @Override
    public double getBruttoMass() {
        return mass + wrapping.getMass();
    }

    public void setMass(double mass) {
        if (mass < 0){
            throw new IllegalArgumentException();
        }
        this.mass = mass;
    }

    public WrappedWeightGoods(WeightGoods goods, Wrapping wrapping, double mass){
        super(goods);
        this.wrapping = new Wrapping(wrapping);
        this.mass = mass;
    }

    public WrappedWeightGoods(String name, String description, Wrapping wrapping, double mass)
    {
        super(name, description);
        this.wrapping = new Wrapping(wrapping);
        this.mass = mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WrappedWeightGoods)) return false;
        if (!super.equals(o)) return false;
        WrappedWeightGoods that = (WrappedWeightGoods) o;
        return Double.compare(that.mass, mass) == 0 &&
                wrapping.equals(that.wrapping);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wrapping, mass);
    }

    @Override
    public String toString(){
        return super.toString() + " (" + wrapping.toString() + ")";
    }
}
