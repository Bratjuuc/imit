package Goods;

public interface MassGetter {
    public double getBruttoMass();
    public double getNettoMass();
//    public String getName();
}
