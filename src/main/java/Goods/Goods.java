package Goods;

import java.util.Objects;

abstract public class Goods {
    private String name;
    private String description;

//    public abstract double getNettoMass();
//    public abstract double getBruttoMass();

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description.trim().length() == 0){
            throw new IllegalArgumentException();
        }
        this.description = description.trim();
    }

    public void setName(String name) {
        if (name.trim().length() == 0){
            throw new IllegalArgumentException();
        }
        this.name = name.trim();
    }

    public Goods(String name, String description){
        this.setDescription(description);
        this.setName(name);
    }

    public Goods(){
        this.setDescription("Sample text");
        this.setName("Sample text");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Goods)) return false;
        Goods goods = (Goods) o;
        return name.equals(goods.name) &&
                description.equals(goods.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString(){
        return name + ": " + description;
    }
}
