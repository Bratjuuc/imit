package Goods;

public class WrappedPiecedGoods extends PiecedGoods implements MassGetter {
    private Wrapping wrapping;
    private int amount;

    public WrappedPiecedGoods(PiecedGoods goods, Wrapping wrapping, int amount){
        super(goods);
        this.wrapping = wrapping;
        if (amount <= 0){
            throw new IllegalArgumentException();
        }
        this.amount = amount;
    }

    @Override
    public double getBruttoMass(){
        return wrapping.getMass() * amount + super.getBruttoMass();
    }

    @Override
    public double getNettoMass(){
        return super.getBruttoMass()*amount;
    }
}
