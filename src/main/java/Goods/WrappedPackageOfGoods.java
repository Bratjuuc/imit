package Goods;

import java.util.Arrays;
import java.util.Objects;

public class WrappedPackageOfGoods extends Goods implements MassGetter{
    private Wrapping wrapping;
    private MassGetter[] array;

    public WrappedPackageOfGoods(String name, String description, Wrapping wr, MassGetter... arr)
    {
        super(name, description);
        array = arr;
        wrapping = new Wrapping(wr);
    }

    public int getLength(){
        return array.length;
    }

    public Goods goodsAt(int i){
        if (i < 0 || i >= array.length){
            throw new IllegalArgumentException();
        }
        return (Goods)array[i];
    }

    @Override
    public  double getNettoMass(){
        double sum = 0;

        for (MassGetter i: array) {
            sum += i.getBruttoMass();
        }
        return sum;
    }

    @Override
    public  double getBruttoMass(){
        double sum = 0;

        for (MassGetter i: array) {
            sum += i.getBruttoMass();
        }
        return sum + wrapping.getMass();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WrappedPackageOfGoods)) return false;
        if (!super.equals(o)) return false;
        WrappedPackageOfGoods that = (WrappedPackageOfGoods) o;
        return wrapping.equals(that.wrapping) &&
                Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), wrapping);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }

    @Override
    public String toString(){
        StringBuilder rest = new StringBuilder("");
        for (MassGetter i: array) {
            rest.append(i.toString());
            rest.append(", ");
        }
        return super.toString() + " (упакованный набор товаров) (" + wrapping.toString() + ") Содержимое: " + rest;
    }
}
