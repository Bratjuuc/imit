package Goods;

public class WeightGoods extends Goods {
    public WeightGoods(String name, String description){
        super(name, description);
    }

    public WeightGoods(){
        super();
    }

    public WeightGoods(WeightGoods init){
        super(init.getName(), init.getDescription());
    }

    @Override
    public String toString() {
        return getName() + "(весовой товар): " + getDescription();
    }
}
