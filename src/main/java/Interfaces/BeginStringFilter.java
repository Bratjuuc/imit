package Interfaces;

public class BeginStringFilter implements Filter {
    private String str;

    public BeginStringFilter(String str){
        this.str = str;
    }

    private boolean isInfixOf(String a, String b){
        if (a.length() > b.length()){
            return false;
        }

        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) != b.charAt(i)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean apply(String str){
        return isInfixOf(this.str, str);
    }
}
