package Interfaces;

public interface Filter {
     boolean apply(String str);
}
