package Interfaces;

public class IsPolyndromeFilter implements Filter {
    //public Filter1(){}
    @Override
    public boolean apply(String str){
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != str.charAt(str.length() - i - 1)){
                return false;
            }
        }
        return true;
    }
}
