package Interfaces;
import Goods.*;

public class GoodsService{

    public static int countByFilter(Filter filter, ConsignmentOfGoods cog){
        int counter = 0;

//        if (filter.apply(cog.getName())){
//            counter++;
//        }
        for (int i = 0; i < cog.getLength(); i++) {
            if (filter.apply(cog.goodsAt(i).getName())){
                counter++;
            }
        }

        return counter;
    }

    public static int countByFilterDeep(Filter filter, ConsignmentOfGoods cog){
        int counter = 0;

//        if (filter.apply(cog.getName())){
//            counter++;
//        }
        for (int i = 0; i < cog.getLength(); i++) {
            if (has(cog.goodsAt(i), filter)){
                counter++;
            }
        }

        return counter;
    }

    private static boolean has(Goods goods, Filter filter){
        if (goods instanceof ConsignmentOfGoods){
            for (int i = 0; i < ((ConsignmentOfGoods)goods).getLength(); i++) {
                if (has(((ConsignmentOfGoods)goods).goodsAt(i), filter)){
                    return true;
                }
            }
            return false;
        }
        if (goods instanceof WrappedPackageOfGoods){
            if (!filter.apply(goods.getName())){
                for (int i = 0; i < ((WrappedPackageOfGoods)goods).getLength(); i++) {
                    if (has(((WrappedPackageOfGoods)goods).goodsAt(i), filter)){
                        return true;
                    }
                }
                return false;
            }
            else return true;
        }
        else{
            return filter.apply(goods.getName());
        }
    }

    public static boolean checkAllWeighted(ConsignmentOfGoods cog){
        for (int i = 0; i < cog.getLength(); i++) {
            if (!(cog.goodsAt(i) instanceof WeightGoods)) {
                return false;
            }
        }
        return true;
    }
}
