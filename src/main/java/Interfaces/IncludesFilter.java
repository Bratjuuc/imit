package Interfaces;

public class IncludesFilter implements Filter {
    private String value;

    public IncludesFilter(String str){
        value = str;
    }

    public IncludesFilter(){
        value = "Sample Text";
    }

    @Override
    public boolean apply(String str){
        int infLen = value.length();

        if (value.length() == 0){
            return true;
        }

        for (int i = 0; i + infLen <= str.length(); i++){
            if (str.substring(i, i + infLen).compareTo(value.toString()) == 0){
                return true;
            }
        }
        return false;
    }
}
