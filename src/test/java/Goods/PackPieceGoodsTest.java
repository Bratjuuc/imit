package Goods;

import Goods.WrappedPackageOfGoods;
import Goods.Wrapping;
import Goods.*;
import org.junit.Test;

public class PackPieceGoodsTest {
    @Test(expected = IllegalArgumentException.class)
    public  void constructorTest(){
        Wrapping winter = new Wrapping("Sample Wrapping", 123);
        WrappedPackageOfGoods pack = new WrappedPackageOfGoods("cool","15", winter);
        PiecedGoods goods = new PiecedGoods("box","cool",15);
        WrappedPackageOfGoods object = new WrappedPackageOfGoods("Sample", "Text", winter, goods,pack);


    }
}
