package Goods;

import Goods.WeightGoods;
import org.junit.Test;

public class CashGoodsTest {
    @Test(expected = IllegalArgumentException.class)
    public  void constructorTest(){
       WeightGoods object = new WeightGoods("","Flowers are pretty");
    }

    @Test(expected = IllegalArgumentException.class)
    public  void constructorTest2(){
        WeightGoods object = new WeightGoods("Sample text","");
    }
}
