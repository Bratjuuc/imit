package Goods;

import Goods.WeightGoods;
import Goods.WrappedWeightGoods;
import Goods.Wrapping;
import org.junit.Test;

public class PackCashGoodsTest {
    @Test(expected = IllegalArgumentException.class)
    public void constructorTest(){
        Wrapping winter = new Wrapping("cool",15);
        WeightGoods cash = new WeightGoods("something","cool");
        WrappedWeightGoods object = new WrappedWeightGoods(cash, winter, 12);
    }
}
