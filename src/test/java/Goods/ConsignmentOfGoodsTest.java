package Goods;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConsignmentOfGoodsTest {

    @Test
    public void testGetMass() {

        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("simple pickle","simple pickle"),pack, 2);
        WrappedPiecedGoods packProduct4 = new WrappedPiecedGoods(new PiecedGoods("keyboard","cool keyboard",12),pack,1);

        ConsignmentOfGoods consignment = new ConsignmentOfGoods(packProduct1,packProduct2,packProduct5,packProduct4);

        assertEquals (1 + (2 + 1) + (2 + 1) + (2 + 1) + 12, consignment.getMass(), 0.00001);
    }

    @Test
    public void testGoodsAt() {
        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("simple pickle","simple pickle"),pack, 2);
        WrappedPiecedGoods packProduct4 = new WrappedPiecedGoods(new PiecedGoods("keyboard","cool keyboard",12), pack,1);

        ConsignmentOfGoods consignment = new ConsignmentOfGoods(packProduct1,packProduct2,packProduct5,packProduct4);
        assertTrue (consignment.goodsAt(1).equals(packProduct2));
    }

}