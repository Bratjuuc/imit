package Interfaces;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ContainsLettersFilterTest {
    @Test
    public  void filterTest(){
        Filter str1 = new IncludesFilter("мама мыла раму");
        Filter str2 = new IncludesFilter("мама не мыла раму");

        assertTrue(str1.apply("мама мыла раму"));
        assertFalse(str2.apply("мама мыла раму"));
        assertFalse(str1.apply("мама не мыла раму"));
        assertTrue(str2.apply("мама не мыла раму"));

    }
    @Test
    public  void filterTest2(){
        Filter str1 = new IsPolyndromeFilter();

        assertTrue(str1.apply("арозаупаланалапуазора"));
        assertTrue(!str1.apply("арозаНЕупаланалапуазора"));

    }
}
