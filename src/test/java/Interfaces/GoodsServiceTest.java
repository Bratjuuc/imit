package Interfaces;


import Interfaces.*;
import Goods.*;
//import Service.ProductService;
import org.junit.Test;

import static org.junit.Assert.*;

public class GoodsServiceTest {
    @Test
    public void countByFilterTest(){
        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle") ,pack, 2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("gray pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct3 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct4 = new WrappedWeightGoods(new WeightGoods("ice cream","yummy ice cream "),pack,2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("cool pickle","weird pickle"), pack,2);

        WrappedPackageOfGoods packaged = new WrappedPackageOfGoods ("sample pickle package", "nothing to say", pack,packProduct1,packProduct2,packProduct3,packProduct5);
        WrappedPackageOfGoods  package2 = new WrappedPackageOfGoods ("Sample pickle package 2","nothing to say", pack,packaged,packProduct2);
        ConsignmentOfGoods consignment = new ConsignmentOfGoods(package2, packProduct1, packProduct2, packProduct3, packProduct4, packProduct5);
        assertEquals(5,GoodsService.countByFilter(new IncludesFilter("pickle"), consignment));

    }
    @Test
    public void countByFilterTestOnEmptyConsigment(){
        ConsignmentOfGoods consignment = new ConsignmentOfGoods();
        assertEquals(0,GoodsService.countByFilter(new IncludesFilter("pickle"),consignment));

    }
    @Test
    public void countByFilterDeepTest(){

        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle"), pack,2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack,2);
        WrappedWeightGoods packProduct3 = new WrappedWeightGoods(new WeightGoods("ice cream","yummy ice cream "), pack,2);
        WrappedWeightGoods packProduct4 = new WrappedWeightGoods(new WeightGoods("blee pickle","simple pickle"),pack,2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("simple pickle","simple pickle"),pack,2);


        WrappedPackageOfGoods packaged = new WrappedPackageOfGoods("pickle box","big box pickle",pack,packProduct3,packProduct1,packProduct2);
        WrappedPackageOfGoods package2 = new WrappedPackageOfGoods("big box pickle box","something yummy",pack,packaged,packProduct4);
        ConsignmentOfGoods consignment = new ConsignmentOfGoods(package2,packaged,packProduct5);
        assertEquals(3,GoodsService.countByFilterDeep(new IncludesFilter("pickle"),consignment));

    }
    @Test
    public  void checkAllWeightTestTrue(){
        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle"), pack,2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack,2);
        WrappedWeightGoods packProduct3 = new WrappedWeightGoods(new WeightGoods("ice cream","yummy ice cream "), pack,2);
        WrappedWeightGoods packProduct4 = new WrappedWeightGoods(new WeightGoods("blue pickle","simple pickle"),pack,2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("simple pickle","simple pickle"),pack,2);


        ConsignmentOfGoods consignment = new ConsignmentOfGoods(packProduct1,packProduct2,packProduct5);
        assertTrue(GoodsService.checkAllWeighted(consignment));

    }
    @Test
    public  void checkAllWeightTestFalse(){
        Wrapping pack = new Wrapping("simple pack ", 1);
        WrappedWeightGoods packProduct1 = new WrappedWeightGoods(new WeightGoods("orange pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct2 = new WrappedWeightGoods(new WeightGoods("pink pickle","weird pickle"), pack, 2);
        WrappedWeightGoods packProduct5 = new WrappedWeightGoods(new WeightGoods("simple pickle","simple pickle"),pack, 2);
        WrappedPiecedGoods packProduct4 = new WrappedPiecedGoods(new PiecedGoods("keyboard","cool keyboard",12),pack, 1);


        ConsignmentOfGoods consignment = new ConsignmentOfGoods(packProduct1,packProduct2,packProduct5,packProduct4);
        assertFalse(GoodsService.checkAllWeighted(consignment));

    }
    @Test
    public  void checkAllWeightTestEmptyConsigment(){
        ConsignmentOfGoods consignment = new ConsignmentOfGoods();
        assertTrue(GoodsService.checkAllWeighted(consignment));
    }
}
