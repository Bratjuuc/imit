package Interfaces;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsPolyndromeFilterTest {

    @Test
    public void apply() {
            Filter str1 = new IsPolyndromeFilter();
            assertTrue(str1.apply("арозаупаланалапуазора"));
            assertFalse(str1.apply("арозаНЕупаланалапуазора"));
    }
}